﻿using System;
using UnityEngine;

namespace PlayerController
{
  public sealed class MovementInput : MonoBehaviour
  {
    public enum Direction
    {
      None,
      Forward,
      Back,
      Left,
      Right
    }

    private struct DirectionData
    {
      public Action CallBack;
      public bool Pressed;
      public readonly KeyCode KeyCode;
      public readonly Direction Direction;

      public DirectionData(KeyCode keyCode, Direction direction) : this()
      {
        Pressed = false;
        KeyCode = keyCode;
        Direction = direction;
      }
    }

    public event Action<Direction> ButtonDown;
    public event Action<Direction> ButtonUp;
    
    private static readonly DirectionData[] _directionsData = new DirectionData[4]
    {
      new DirectionData(_forwardKeyCode, Direction.Forward),
      new DirectionData(_backKeyCode, Direction.Back),
      new DirectionData(_leftKeyCode, Direction.Left),
      new DirectionData(_rightKeyCode, Direction.Right)
    };

    private const KeyCode _forwardKeyCode = KeyCode.W;
    private const KeyCode _backKeyCode = KeyCode.S;
    private const KeyCode _leftKeyCode = KeyCode.L;
    private const KeyCode _rightKeyCode = KeyCode.R;

    private void Update()
    {
      foreach (DirectionData directionData in _directionsData)
      {
        KeyDown(directionData);
        KeyUp(directionData);
      }
    }

    private void KeyDown(DirectionData directionData)
    {
      if (Input.GetKeyDown(directionData.KeyCode))
      {
        directionData.Pressed = true;
        directionData.CallBack?.Invoke();
        ButtonDown?.Invoke(directionData.Direction);
      }
    }

    private void KeyUp(DirectionData directionData)
    {
      if (Input.GetKeyDown(directionData.KeyCode))
      {
        directionData.Pressed = false;
        directionData.CallBack?.Invoke();
        ButtonUp?.Invoke(directionData.Direction);
      }
    }
  }
}